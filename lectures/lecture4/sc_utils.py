import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import mpl_toolkits.axes_grid1
from matplotlib.animation import FuncAnimation

# class for creating an interactive player
# see here for more details: 
# https://stackoverflow.com/questions/46325447/animated-interactive-plot-using-matplotlib 

class Player(FuncAnimation):
    def __init__(self, fig, func, play, init_state1, init_state2, frames=None, init_func=None, fargs=None,
                 save_count=None, pos=(0.125, 0.92), **kwargs):
        self.i = 0
        self.k = 5e0
        self.ball1 = init_state1.copy()
        self.ball2 = init_state2.copy()
        self.init_state1 = init_state1
        self.init_state2 = init_state2
        self.restart = False
        self.max=1000
        self.fig = fig
        self.func = func
        self.setup(pos)
        FuncAnimation.__init__(self,self.fig, self.func, frames=play(self), 
                                           init_func=init_func, fargs=fargs,
                                           save_count=save_count,
                                           cache_frame_data= False, **kwargs )    

    def to_start_state(self):
        # reset state if k has changed
        if not self.restart:
            return
        self.restart = False
        self.i = 0
        self.ball1['position'], self.ball1['velocity'], self.ball1['force'] = self.init_state1['position'], self.init_state1['velocity'], self.init_state1['force']
        self.ball2['position'], self.ball2['velocity'], self.ball2['force'] = self.init_state2['position'], self.init_state2['velocity'], self.init_state2['force']
        # self.event_source.start()

    def start(self, event=None):
        self.resume()

    def stop(self, event=None):
        self.pause()

    def setup(self, pos):
        playerax = self.fig.add_axes([pos[0],pos[1], 0.64, 0.04])
        #make plaxerax invisible
        playerax.set_frame_on(False)
        playerax.get_xaxis().set_visible(False)
        playerax.get_yaxis().set_visible(False)
        divider = mpl_toolkits.axes_grid1.make_axes_locatable(playerax)
        sax = divider.append_axes("right", size="80%", pad=0.1)
        fax = divider.append_axes("right", size="80%", pad=0.1)
        sliderax = divider.append_axes("right", size="500%", pad=0.2)

        self.button_stop = matplotlib.widgets.Button(sax, label='$\u25A0$')
        self.button_start = matplotlib.widgets.Button(fax, label='$\u25B6$')
        self.button_stop.on_clicked(self.stop)
        self.button_start.on_clicked(self.start)
        
        self.slider = matplotlib.widgets.Slider(sliderax, '', 
                                                5, 500, valinit=self.k, valstep=1)
        self.slider.on_changed(self.set_k)
        self.slider.set_val(self.k)

    def set_k(self,i):
        self.k = int(self.slider.val)
        self.restart = True
        self.to_start_state()