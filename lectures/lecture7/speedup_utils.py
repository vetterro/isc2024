import math
import numpy as np
import time
import multiprocessing as mp
import copy

def sort(arr):
    for i in range(len(arr)):
        for j in range(i, len(arr)):
            if arr[i] > arr[j]:
                arr[i], arr[j] = arr[j], arr[i]
    return arr

def execute_parallel(strong_scaling):
    # Initialize lists to hold timing results
    times = []

    # Loop over several process counts, up to 12 processes
    for i in range(1, min(mp.cpu_count(), 12) + 1):  # The min function is used to prevent asking for more processes than available
        # loop k times over each process to mitigate runtime fluctuations
        k = 5
        time_sum = 0
        for _ in range(k):
            if strong_scaling:
                inputs = np.random.randint(1, 5000, (8000, 50))
            else:
                inputs = np.random.randint(1, 5000, (1000*i, 50))
            # Create a process pool
            pool = mp.Pool(processes=i)
            # Start the timer
            start_time = time.time()
            # Execute the function in parallel
            results = pool.map(sort, inputs)
            # Stop the timer
            end_time = time.time()
            time_sum += end_time - start_time
            # Close the pool
            pool.close()
            pool.join()
        # Record average elapsed time
        times.append(time_sum / k)

    return times