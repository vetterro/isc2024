import numpy
import math
from matplotlib import pyplot
from matplotlib import animation
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib import cm

D = 1.0 # initial cell diameter
K = 1.0 # area stiffness
Gamma = 1.0/30.0 # perimeter stiffness
gamma = -1.0/20.0 # line tension (can be negative)
t_end = 2.0 # simulation end time
h = 0.001 # pseudo-timestep
eta = 0.01 # viscous damping coefficient
Nc = 5 # number of cells in each direction
G = 2.0 # linear area growth rate

duration = 10 # video duration in secs
fps = 25 # frames per second
frames = duration * fps # number of frames
stepsPerFrame = int(round(t_end/h/frames)) # timesteps per animation frame

class Vertex:
  def __init__(self, x):
    self.x = x # position
    self.f = numpy.zeros(2) # force
    
  def clearForce(self):
    self.f = numpy.zeros(2)
    
  def makeStep(self):
    # forward Euler pseudo-timestepping
    self.x += h/eta * self.f

class Cell:
  def __init__(self):
    self.v = [] # indices of vertices belonging to this cell
    self.A = 0 # cell area
    self.P = 0 # cell perimeter
    self.A0 = 0 # target area
  
  def computeArea(self, vertices):
    self.A = 0
    n = len(self.v)
    for k in range(n):
      i = self.v[k]
      j = self.v[(k + 1) % n]
      self.A += vertices[i].x[0] * vertices[j].x[1] - vertices[i].x[1] * vertices[j].x[0]
    self.A /= 2
  
  def computePerimeter(self, vertices):
    self.P = 0
    n = len(self.v)
    for k in range(n):
      i = self.v[k]
      j = self.v[(k + 1) % n]
      self.P += numpy.linalg.norm(vertices[i].x - vertices[j].x)
  
  def computeForce(self, vertices):
    self.computeArea(vertices)
    self.computePerimeter(vertices)
    n = len(self.v)
    for l in range(n):
      i = self.v[l]
      j = self.v[(l - 1) % n]
      k = self.v[(l + 1) % n]
      e1 = vertices[j].x - vertices[i].x
      e2 = vertices[k].x - vertices[i].x
      e3 = vertices[k].x - vertices[j].x
      gradA = 0.5 * numpy.array([e3[1], -e3[0]])
      n1 = e1 / numpy.linalg.norm(e1)
      n2 = e2 / numpy.linalg.norm(e2)
      gradP = -n1 - n2
      vertices[i].f -= K * (self.A - self.A0) * gradA
      vertices[i].f -= Gamma * self.P * gradP
      vertices[i].f -= gamma * gradP
  
  def grow(self):
    self.A0 += G * h

# initialize the cells & vertices
cells = []
vertices = []
d = math.sqrt(3)/2 # ratio of inscribed to circumscribed circle diameters
for i in range(Nc):
  for j in range(Nc):
    cells.append(Cell())
    for k in range(6):
      x = numpy.array([0.75*i, (j + 0.5*(i%2)) * d])
      x += numpy.array([math.cos(k*math.pi/3), math.sin(k*math.pi/3)]) / 2
      vertices.append(Vertex(D * x))
      cells[-1].v.append(len(vertices) - 1)

# merge vertices that are shared between cells
i = 0
while i < len(vertices):
  j = i+1
  while j < len(vertices):
    if numpy.linalg.norm(vertices[i].x - vertices[j].x) < D/100:
      for c in cells:
        for k in range(len(c.v)):
          if c.v[k] == j:
            c.v[k] = i
          elif c.v[k] > j:
            c.v[k] -= 1
      del vertices[j]
    else:
      j += 1
  i += 1

# add randomness
numpy.random.seed(42)
for v in vertices:
  v.x += D/10 * numpy.array([numpy.random.uniform(-1, 1), numpy.random.uniform(-1, 1)])

# initialize the target cell areas
for c in cells:
  c.computeArea(vertices) # compute the initial area
  c.A0 = c.A # use this as the target area

# figure settings
margin = 2
fig = pyplot.figure()
ax = pyplot.axes(xlim=(-margin*D, ((Nc-1)*0.75+margin)*D), ylim=(-margin*D, ((Nc-0.5)*d+margin)*D), xlabel='x', ylabel='y')
ax.set_aspect('equal', 'box')
t = ax.text(-0.9*margin*D, -0.9*margin*D, 't=0')
polygons = []
for c in cells:
  polygons.append(Polygon(xy=[vertices[v].x for v in c.v], closed=True))
collection = PatchCollection(polygons, cmap=cm.jet, edgecolor="black")
collection.set_array(numpy.array([len(c.v) for c in cells]))
p = ax.add_collection(collection)
collection.set_clim([3,9])
pyplot.colorbar(collection, ticks=range(10), label='neighbors')

# main loop
def run(frame):
  for s in range(stepsPerFrame):
    # grow the cell in the middle
    cells[(Nc + 1) * int(Nc / 2)].grow()

    for v in vertices:
      v.clearForce()

    for c in cells:
      c.computeForce(vertices)
    
    for v in vertices:
      v.makeStep()
    
  # plot
  patches = []
  for c in cells:
    patches.append(Polygon(xy=[vertices[v].x for v in c.v], closed=True))
  collection.set_paths(patches)
  collection.set_array(numpy.array([len(c.v) for c in cells]))
  t.set_text('t = ' + str(frame * stepsPerFrame * h))

anim = animation.FuncAnimation(fig, run, frames=frames, interval=1000/fps)
anim.save('vertex_model_basic.mp4', fps=fps, extra_args=['-vcodec', 'libx264'])
