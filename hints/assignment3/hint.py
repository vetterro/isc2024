n = 5 # number of points in each direction
N = n * n # number of mass points
M = (n-1) * (3*n-1) # number of springs

class Point:
  def __init__(self):
    self.x = numpy.zeros(2)
    ...
  
class Spring:
  def __init__(self):
    self.p1 = 0 # index of first point
    self.p2 = 0 # index of second point
    ...

points = [Point() for i in range(N)]
springs = [Spring() for j in range(M)]

# place the mass points
for i in range(n):
  for j in range(n):
    points[i*n+j].x = numpy.array([j*L + i%2 * L/2, i*L * math.sqrt(3)/2])

# connect the points by springs
for i in range(n):
  for j in range(n-1):
    s1 = i*(n-1)+j
    s2 = n*(n-1) + s1
    springs[s1].p1 = i*n+j
    springs[s1].p2 = i*n+j+1
    springs[s2].p1 = j*n+i
    springs[s2].p2 = (j+1)*n+i

for i in range(n-1):
  for j in range(n-1):
    s = i*(n-1)+j + 2*n*(n-1)
    springs[s].p1 = i*n+j + (i+1) % 2
    springs[s].p2 = (i+1)*n+j + i % 2
