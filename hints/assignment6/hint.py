import numpy
import math

Nc = 5 # number of cells in each direction
D = 1.0 # initial cell diameter

class Vertex:
  def __init__(self, x):
    self.x = x # position
    self.f = numpy.zeros(2) # force
    ...
  
class Cell:
  def __init__(self):
    self.v = [] # indices of vertices belonging to this cell
    ...

# initialize the cells & vertices
cells = []
vertices = []
d = math.sqrt(3)/2 # ratio of inscribed to circumscribed circle diameters
for i in range(Nc):
  for j in range(Nc):
    cells.append(Cell())
    for k in range(6):
      x = numpy.array([0.75*i, (j + 0.5*(i%2)) * d])
      x += numpy.array([math.cos(k*math.pi/3), math.sin(k*math.pi/3)]) / 2
      vertices.append(Vertex(D * x))
      cells[-1].v.append(len(vertices) - 1)

# merge vertices that are shared between cells
i = 0
while i < len(vertices):
  j = i+1
  while j < len(vertices):
    if numpy.linalg.norm(vertices[i].x - vertices[j].x) < D/100:
      for c in cells:
        for k in range(len(c.v)):
          if c.v[k] == j:
            c.v[k] = i
          elif c.v[k] > j:
            c.v[k] -= 1
      del vertices[j]
    else:
      j += 1
  i += 1

# add randomness
for v in vertices:
  v.x += D/10 * numpy.array([numpy.random.uniform(-1, 1), numpy.random.uniform(-1, 1)])
